from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from todos.models import TodoList, TodoItem
from django.shortcuts import redirect
from django.urls import reverse_lazy


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolistlist"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todolistdetail"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    context_object_name = "todolistcreate"
    fields = ["name"]

    def form_valid(self, form):
        todo = form.save(commit=False)
        todo.owner = self.request.user
        todo.save()
        return redirect("todo_list_detail", pk=todo.id)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    context_object_name = "todolistedit"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    context_object_name = "todolistdelete"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todoitem/new.html"
    context_object_name = "todoitemcreate"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todoitem/edit.html"
    context_object_name = "todoitemedit"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
